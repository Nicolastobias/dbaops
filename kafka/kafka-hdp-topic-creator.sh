#!/bin/bash
#Nicolas Tobias ## Marzo-2017
#Topics creator
################################################################################

$zk=$1

echo " Creation of kafka-topics"
./kafkabin/kafka-topics.sh --create --zookeeper $zk:2181 --topic transactions --config retention.ms=86400000
./kafkabin/kafka-topics.sh --create --zookeeper $zk:2181 --topic account_activities --config retention.ms=86400000
./kafkabin/kafka-topics.sh --create --zookeeper $zk:2181 --topic account_sources --config retention.ms=86400000
./kafkabin/kafka-topics.sh --create --zookeeper $zk:2181 --topic session_creations --config retention.ms=86400000
./kafkabin/kafka-topics.sh --create --zookeeper $zk:2181 --topic session_terminations --config retention.ms=86400000
./kafkabin/kafka-topics.sh --create --zookeeper $zk:2181 --topic customer_updates --config retention.ms=86400000
./kafkabin/kafka-topics.sh --create --zookeeper $zk:2181 --topic registration_updates --config retention.ms=86400000
./kafkabin/kafka-topics.sh --create --zookeeper $zk:2181 --topic bonus_definition_updates --config retention.ms=86400000
