#!/usr/bin/perl -w
#Nicolas Tobias ## Marzo-2017
#kafkaMirror-Wrapper

use strict;
use warnings;
use Proc::Daemon;
Proc:Daemon::Init;

################################################################################
################################################################################
#HOWTO - TODO USING AppConfig.pm
#This is a wrapper for the kafkaMirror service between environments and the HDP-DW-1
#Is you need to change something, stop the service and change the configfile for every
#environment.

#pro2 kMirror-pro2.config
#pro3 kMirror-pro3.config
#pro4 kMirror-pro4.config

################################################################################
################################################################################

#Enviroments for the Daemon
$daemon = Proc::Daemon->new(
    work_dir      => '/opt/kafkaMirror/daemon',
    child_STOUDT  => '/opt/kafkaMirror/daemon/output.log',
    child_STDERR  => '+>>error.log'
    pid_file      => 'daemon.pid',
    exec_command  => "$exec",
);

#Enviroment variables for the wrapper
my $vConsumer="/opt/kafkaMirror/pro2_mirror_consumer.conf";
my $vProducer="/opt/kafkaMirror/pro2_mirror_producer.conf";
my $vTopic="transactions,account_activities,account_sources,session_creations,session_terminations,customer_updates,registration_updates,bonus_definition_updates";
my $vKafkaMirror="/usr/hdp/current/kafka-broker/bin/kafka-run-class.sh kafka.tools.MirrorMaker";

################################################################################
################################################################################

#Run kafkaMirror
my $exec = "/usr/bin/screen -S $vScreen";
$exec .= " $vKafkaMirror";
$exec .= " --consumer.config $vConsumer";
$exec .= " --producer.config $vProducer";
$exec .= " --whitelist=$sTopics";

$
#test
#print $exec;

#enable to make it run
system($exec);
exit 0;
