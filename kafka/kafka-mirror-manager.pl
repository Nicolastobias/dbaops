#!/usr/bin/perl -w
#Nicolas Tobias ## Marzo-2017
#Perl Script to manage kafkaMirror
################################################################################
my $vScreen=$ARGV[0];
my $vConsumer=$ARGV[1];
my $vProducer=$ARGV[2];
my $vTopic=$ARGV[3];

my $vKafkaMirror="/usr/hdp/current/kafka-broker/bin/kafka-run-class.sh kafka.tools.MirrorMaker";
#check number og arguments
my $check = $#ARGV + 1;
print $check;

#if argument less than mandatory
if ($check != 4) {
    print "\nUsage: perl ./kafka-mirror-manager.pl [screen name][consumer config path] [producer config path] [topics whitelist path] \n";
    exit;
}

#Fill with topic
#open topics file
open (my $vFH,'<:encoding(UTF-8)', $vTopic) or die "file containing topics ERROR!\n";
while (<$vFH>){
    chomp $_;
    push (@topics, $_);

}
my $sTopics = join ',', @topics;
#Run kafkaMirror
my $exec = "/usr/bin/screen -S $vScreen";
$exec .= " $vKafkaMirror";
$exec .= " --consumer.config $vConsumer";
$exec .= " --producer.config $vProducer";
$exec .= " --whitelist=$sTopics";

#test
#print $exec;

#enable to make it run
system($exec);
exit 0;
