#!/bin/bash
#Nicolas Tobias ## Marzo-2017
#Simple Wrapper
################################################################################


echo 'installing daemonize is not installed...'
yum install daemonize -y

echo 'Executing PRO2 KafkaMirror..'
daemonize -c /opt/kafkaMirror \
-e /opt/kafkaMirror/kafkaMirror.error.log \
-o /opt/kafkaMirror/kafkaMirror.ouput.log \
-p /opt/kafkaMirror/kafkaMirror.pid \
-l /opt/kafkaMirror/kafkaMirror.lock \
/usr/hdp/current/kafka-broker/bin/kafka-run-class.sh kafka.tools.MirrorMaker \
--consumer.config /opt/kafkaMirror/pro2_mirror_consumer.conf \
--producer.config /opt/kafkaMirror/pro2_mirror_producer.conf \
--whitelist="transactions,account_activities,account_sources,session_creations,session_terminations,customer_updates,registration_updates,bonus_definition_updates"
