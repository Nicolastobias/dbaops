ZK=$1

#Default values for retention, partition and replication-factor
retention=${2:-6048000000}
partitions=${3:-3}
replication=${4:-3}


topics=( account_activities account_sources bonus_definition_updates btags customer_updates registration_updates session_creations session_terminations )

echo "Creating topics for $ZK with: Retention = $retention || Partitions = $partitions || Replication = $replication"
for i in "${topics[@]}"
do
echo "Creation of topic > $i"
./kafka-topics.sh --create --zookeeper $ZK:2181 --topic $i --config retention.ms=$retention  --partitions $partitions --replication-factor $replication --disable-rack-aware
sleep 3
done
